<!DOCTYPE html>
<html lang="ru">


  <head>
      <meta charset="utf-8"/>
       <title>Kikoriki</title>
       <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
       <link href="style.css" rel="stylesheet">
       <link href="https://c7.hotpng.com/preview/7/64/896/computer-icons-shutdown-button-logo-power.jpg" rel="icon">
  </head>

  <body>
    <div class="container-lg px-0">
      <div class="main row mx-auto" >
          <div id="user_form" class="cal-12 mx-auto">
          <h2 id="form"> ФОРМА </h2>
          <form action="." method="POST">
            <label class="textStyle">
              Введите имя 
              <br>
              <input name="name" value="Этим мальчиком был Альберт Эйнштейн">
            </label>
            <br>
            <label class="textStyle">
              Введите eMail
              <br>
              <input name="email" value="Your_Mail@mail.ru" type="email">
            </label>
            <br>
            <label>
              Дата рождения:<br/>
              <input name="birthday" value="2020-09-14" type="date"/>
            </label>
            <br/>
            <label class="textStyle">
              Выберите пол
              <br>
              <input type="radio" checked="checked" name="pol" value="M">
              Муж
            </label>
            <label class="textStyle">
              <input type="radio" name="pol" value="F">
               Жен
            </label>
            <br>
            <label class="textStyle">
              Выберите кол-во конечностей
              <br>
              <input type="radio" name="kon" value="3">
              3
            </label>
            <br>
            <label class="textStyle">
              <input type="radio" checked="checked" name="kon" value="4">
              4
            </label>
            <br>
            <label class="textStyle">
              <input type="radio" name="kon" value="2.5">
               2,5
            </label>
            <br>
            <label class="textStyle">
              <input type="radio" name="kon" value="Grivus">
               6 (General Kenobi)
            </label>
            <br>
            <label class="textStyle">
              Выберите свои сверхспособности
              <br>
              <select name="sverhspos[]" multiple="multiple">
                <option value="1">Бессметрие</option>
                <option value="2">Прохождение сквозь стены</option>
                <option value="3">Левитация</option>
                <option value="4">Телепатия</option>
              </select>
            </label>
            <br>
            <label class="textStyle">
              Биография:
              <br>
              <textarea name="bio">Однажды, далеко-далеко на востоке...</textarea>
            </label>
            <br>
            <label class="textStyle">
              <input type="checkbox" name="soglasie"> С контрактом ознакомлен
            </label>
            <br>
            <input type="submit" value="Отправить">
          </form>
          </div>
      </div>
    </div>  
    <a id="end"></a>
		<footer id="footer_of_my_site">
			<b>Вы добрались до конца Internet</b>
    </footer>   
  </body>  
</html>
