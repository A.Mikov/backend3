<?php
header('Content-Type: text/html; charset=UTF-8');

//Only report fatal errors and parse errors.
error_reporting(E_ERROR | E_PARSE);

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Спасибо, результаты сохранены.');
  }
  include('form.php');
    exit();
}

$errors = FALSE;

$errors_message = "Неверно заполнены следующие поля: <br/>";

if (empty($_POST['name'])) {
  $errors_message.= 'Имя.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $_POST['email'])) {
  $errors_message.= 'Email.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['birthday'])) {
  $errors_message.= 'Дата рождения.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[MF]$|^(NB)$/', $_POST['pol'])) {
  $errors_message.= 'Пол.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[0-6]$/', $_POST['kon'])) {
  $errors_message.= 'Количество конечностей.<br/>';
  $errors = TRUE;
}
if (!isset($_POST['soglasie'])) {
  $errors_message.= 'Вы не ознакомились с контрактом.<br/>';
  $errors = TRUE;
}

if ($errors) { 
  include('form.php');
  print($errors_message);          
  exit();
}

$user = 'u36993';
$pass = '3932198';
$db = new PDO('mysql:host=localhost;dbname=u36993', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $db->beginTransaction();
  $stmt1 = $db->prepare("INSERT INTO FORM SET name = ?, email = ?, bd = ?, 
    pol= ? , kon = ?, bio = ?");
  $stmt1 -> execute([$_POST['name'], $_POST['email'], $_POST['birthday'], 
   $_POST['pol'],$_POST['kon'], $_POST['bio']]);
  $stmt2 = $db->prepare("INSERT INTO user_sups SET user_id = ?, id_sup = ?");
  $id = $db->lastInsertId();
  foreach ($_POST['sverhspos'] as $s)
    $stmt2 -> execute([$id, $s]);
  $db->commit();
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  $db->rollBack();
  exit();
}


header('Location: ?save=1');
